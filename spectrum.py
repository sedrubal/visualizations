#!/usr/bin/env python3
import curses
import wave

import numpy as np
import pyaudio

CHUNK = 1024
FORMAT = pyaudio.paInt16
SAMPWIDTH = 2  # Int16: 2 bytes
RATE = 44100
ANIMATION = 1
OLD_FFT_ALPHA = .6  # alpha for moving average with old fft values

COLOR_THEME = [
    # (foreground, background)
    (curses.COLOR_WHITE, curses.COLOR_BLACK),
    (curses.COLOR_YELLOW, curses.COLOR_BLACK),
    (curses.COLOR_RED, curses.COLOR_BLACK),
    (curses.COLOR_MAGENTA, curses.COLOR_BLACK),
    # (curses.COLOR_BLUE, curses.COLOR_BLACK),
    (curses.COLOR_CYAN, curses.COLOR_BLACK),
    (curses.COLOR_GREEN, curses.COLOR_BLACK),
]

def main():
    # use a Blackman window
    window = np.blackman(CHUNK)
    # open stream
    p = pyaudio.PyAudio()
    stream = p.open(
        format=FORMAT,
        channels=1,
        rate=RATE,
        output=False,
        input=True
    )

    # initialize ncurses
    scr = curses.initscr()
    curses.curs_set(0)
    curses.start_color()
    for i, color_pair in enumerate(COLOR_THEME):
        curses.init_pair(1 + i, *color_pair)
    # curses.use_default_colors()
    # for i in range(0, curses.COLORS):
    #     curses.init_pair(i + 1, i, -1)

    max_val = 1
    old_fft = None

    def plot_fft(fft):
        """Plot a fft to terminal"""
        nonlocal max_val
        nonlocal old_fft
        max_val *= 0.5  # re-increase bar height even if song gets more quiet
        height, width = scr.getmaxyx()

        OFFSET = 256  # min freq is 256 # TODO 256???
        a = (width + OFFSET) / np.log2(fft.shape[0])
        # fft[int(np.floor(2**((i + OFFSET) / a) - 1))] += sum(fft[0 : int(np.floor(2**((i + OFFSET) / a) - 1))]) // OFFSET
        fft = list(map(sum, (
            fft[int(np.floor(2**((i + OFFSET) / a) - 1)) : int(np.floor(2**((i + OFFSET + 1) / a) - 1))]
            for i in range(width)
        )))
        # fft = [fft[2**i - 1 : 2**(i + 1) - 1] for i in range(int(np.ceil(np.log2(fft.shape[0]))))]
        # fft = np.array(list(map(sum, fft)))
        # print(fft.shape[0])
        # make f axis logarithmix (log2) for human understanding of frequences and aggregate these inequdistant steps
        ##### fft = fft[1:RATE // 150]  # high frequency are not so interesting  # -> x axis in log2 is even better

        # # aggregate into (screen) width frequence ranges:
        # fft = fft[:width * (fft.shape[0] // width)]
        # fft = fft.reshape((width, fft.shape[0] // width)).sum(axis=1)

        # highpass:
        # fft = np.array([fft[f] * f for f in range(fft.shape[0])])

        # fft = np.array(np.log10(np.array(fft) + 1) * 10, dtype=int)  # human loudness recognition is log10 -> consumes too much CPU

        # norm values to current maximum:
        max_val = max(max_val, max(fft))  # TODO mute background noise (2**40?)
        scale = height / max_val
        if ANIMATION == 2: scale /= 2
        fft = np.array(scale * np.array(fft), dtype='int')

        # include old values using moving average
        if old_fft is None:
            old_fft = np.zeros(fft.shape[0])
        fft = OLD_FFT_ALPHA * old_fft + (1 - OLD_FFT_ALPHA) * fft
        old_fft = fft

        scr.clear()
        for y in range(height):
            for x in range(width - 1):
                if fft[x] >= (height if ANIMATION == 1 else height // 2) - y:
                    c = (y * len(COLOR_THEME)) // height
                    try:
                        if ANIMATION == 1:
                            scr.addch(y, x, ord('#'), curses.color_pair(((y - 1) * (len(COLOR_THEME))) // height + 1))
                        elif ANIMATION == 2:
                            scr.addch(y, x, ord('#'), curses.color_pair(c))
                            scr.addch(height - y - 1, x, ord('#'), curses.color_pair(c))
                            if y > height // 2: break
                    except Exception:
                        # DEBUG
                        curses.nocbreak()
                        scr.keypad(0)
                        curses.echo()
                        curses.endwin()
                        stream.close()
                        p.terminate()
                        print(
                            'x', x,
                            'y', y,
                            'c', c,
                            'h', height,
                            'w', width,
                        )
                        exit()

        scr.refresh()

    # read some da
    # play stream and find the frequency of each CHUNK
    try:
        while True:
            data = stream.read(CHUNK)

            # unpack the data and times by the hamming window
            indata = np.array(wave.struct.unpack("%dh"%(len(data)/SAMPWIDTH),\
                                                 data))*window
            # Take the fft and square each value
            fftData=abs(np.fft.rfft(indata))**2
            plot_fft(fftData)
    except KeyboardInterrupt:
        pass
    finally:
        curses.nocbreak()
        scr.keypad(0)
        curses.echo()
        curses.endwin()
        stream.stop_stream()
        stream.close()
        p.terminate()

if __name__ == '__main__':
    main()
