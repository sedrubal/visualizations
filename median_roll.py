#!/usr/bin/env python3
import shutil
import sys
import wave

import numpy as np
import pyaudio

CHUNK = 1024
FORMAT = pyaudio.paInt16
CHANNELS = 1
RATE = 44100
# MEASURE_RANGE = 100
MEASURE_RANGE = shutil.get_terminal_size().columns


class RingBuffer():
    """A 1D ring buffer using numpy arrays."""
    def __init__(self, length):
        self.data = np.zeros(length)
        self.index = 0
        self.length = length

    def add(self, item):
        """Add an item to the ringbuffer."""
        self.data[self.index] = item
        self.index = (self.index + 1) % self.length

    def __getitem__(self, index):
        """Get element on (relative) index."""
        if isinstance(index, slice):
            return self.data[
                self.index + index.start:
                min(self.length - 1, self.index + index.end):
                index.step
            ] + self.data[
                0:
                min(self.length, index.end - self.index):
                index.step
            ]
        return self.data[(self.index + index) % self.length]

    def to_list(self):
        """Return self as numpy array."""
        return np.roll(self.data, -self.index - 1)

    def __repr__(self):
        return str(self.data) + ' Index: ' + str(self.index)

RBUF = RingBuffer(MEASURE_RANGE)

p = pyaudio.PyAudio()
print(chr(27) + "[2J")  # clear terminal

stream = p.open(
    format=FORMAT,
    channels=CHANNELS,
    rate=RATE,
    input=True,
    frames_per_buffer=CHUNK
)

# frames = np.zeros(int(np.ceil(RATE * RECORD_SECONDS / CHUNK) * CHUNK * CHANNELS), dtype='byte')
frames = []
try:
    while True:
        data = stream.read(CHUNK)
        frames = np.array(wave.struct.unpack("%dh"%(len(data)/2), data))
        m = np.sqrt(sum(np.square(frames, dtype='int32')) / len(frames))
        RBUF.add(m)
        buf = RBUF.to_list()
        buf = buf > np.median(buf)
        print('\r' + ''.join(('#' if v else ' ' for v in buf)), end='')
except KeyboardInterrupt:
    print('\rBye')
    stream.stop_stream()
    stream.close()
    p.terminate()
