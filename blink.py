#!/usr/bin/env python3
import sys
import wave
import curses
import random

import numpy as np
import pyaudio

CHUNK = 1024
FORMAT = pyaudio.paInt16
CHANNELS = 1
RATE = 44100
CHUNKS_BETWEEN_CHANGES = 10
BEAT_THRESHOLD = 75  # current sample has to be louder than xx% percent of the other samples
ANTIBEAT_THRESHOLD = 25  # current sample must be more quiet than xx% of the other samples

COLOR_THEME = [
    # (foreground, background)
    (curses.COLOR_BLACK, curses.COLOR_MAGENTA),
    (curses.COLOR_BLACK, curses.COLOR_CYAN),
    (curses.COLOR_BLACK, curses.COLOR_GREEN),
    (curses.COLOR_BLACK, curses.COLOR_YELLOW),
    (curses.COLOR_BLACK, curses.COLOR_RED),
    (curses.COLOR_BLACK, curses.COLOR_WHITE),
]

RBUF = np.zeros(50, dtype='int')

p = pyaudio.PyAudio()

stream = p.open(
    format=FORMAT,
    channels=CHANNELS,
    rate=RATE,
    input=True,
    frames_per_buffer=CHUNK
)

SCR = curses.initscr()
curses.curs_set(0)
curses.start_color()
for i, color_pair in enumerate(COLOR_THEME):
    curses.init_pair(1 + i, *color_pair)
max_val = 0


def give_me_a_random_number(lower_than=0, which_is_not=0):
    num = random.randint(0, lower_than)
    if num == which_is_not:
        return (num + 1) % lower_than
    return num

CUR_COLOR = 0
def change_color():
    """Plot the values to the screen as vertical bars."""
    scr_height, scr_width = SCR.getmaxyx()
    global CUR_COLOR
    CUR_COLOR = give_me_a_random_number(lower_than=len(COLOR_THEME), which_is_not=CUR_COLOR)
    for x in range(scr_width - 1):
        for y in range(scr_height):
            SCR.addch(y, x, ord(' '), curses.color_pair(CUR_COLOR))
    SCR.refresh()


try:
    max_percentil = 0
    we_detected_an_antibeat_recently = True
    while True:
        data = stream.read(CHUNK)
        frames = np.array(wave.struct.unpack("%dh"%(len(data)/2), data))
        m = int(np.sqrt(sum(np.square(frames, dtype='int32')) / len(frames)))
        RBUF[-1] = m
        # check if there was a beat
        max_percentil = max(max_percentil, np.percentile(RBUF, BEAT_THRESHOLD)) * 0.99
        now_we_have_a_beat = RBUF[-1] > max_percentil
        if we_detected_an_antibeat_recently and now_we_have_a_beat:
            # last_change_was_n_chunks_ago = 0
            change_color()
            we_detected_an_antibeat_recently = False
        elif not we_detected_an_antibeat_recently:
            we_detected_an_antibeat_recently = RBUF[-1] < np.percentile(RBUF, ANTIBEAT_THRESHOLD)
        # last_change_was_n_chunks_ago += 1
        RBUF = np.roll(RBUF, -1)
except KeyboardInterrupt:
    curses.nocbreak()
    SCR.keypad(0)
    curses.echo()
    curses.endwin()
    stream.stop_stream()
    stream.close()
    p.terminate()
