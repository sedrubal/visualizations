#!/usr/bin/env python3
import shutil
import sys
import wave

import numpy as np
import pyaudio

CHUNK = 1024
FORMAT = pyaudio.paInt16
CHANNELS = 1
RATE = 44100

p = pyaudio.PyAudio()
print(chr(27) + "[2J")  # clear terminal

stream = p.open(
    format=FORMAT,
    channels=CHANNELS,
    rate=RATE,
    input=True,
    frames_per_buffer=CHUNK
)

# frames = np.zeros(int(np.ceil(RATE * RECORD_SECONDS / CHUNK) * CHUNK * CHANNELS), dtype='byte')
frames = []
try:
    while True:

        data = stream.read(CHUNK)
        frames = np.array(wave.struct.unpack("%dh"%(len(data)/2), data))
        # m = sum(np.abs(frames)) / len(frames) / 128
        m = np.sqrt(sum(np.square(frames, dtype='int32')) / len(frames)) / 2**15
        col = shutil.get_terminal_size().columns
        col -= len('___%[ ]')
        bar_width = m * col
        print('\r{prog: >3}%[{bar}{half}{white}]'.format(
            prog=int(m * 100),
            bar=int(bar_width) * '=',
            half='-' if bar_width % 1 > .5 else ' ',
            white=' ' * (col - int(bar_width) - 1),
        ), end='', file=sys.stderr)
except KeyboardInterrupt:
    print('\rBye')
    stream.stop_stream()
    stream.close()
    p.terminate()
