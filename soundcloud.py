#!/usr/bin/env python3
import sys
import wave
import curses

import numpy as np
import pyaudio

CHUNK = 1024 * 2
FORMAT = pyaudio.paInt16
CHANNELS = 1
RATE = 44100
# MEASURE_RANGE = 100

COLOR_THEME = [
    # (foreground, background)
    (curses.COLOR_MAGENTA, curses.COLOR_BLACK),
    (curses.COLOR_CYAN, curses.COLOR_BLACK),
    (curses.COLOR_GREEN, curses.COLOR_BLACK),
    (curses.COLOR_YELLOW, curses.COLOR_BLACK),
    (curses.COLOR_RED, curses.COLOR_BLACK),
    (curses.COLOR_WHITE, curses.COLOR_BLACK),
]

RBUF = np.zeros(0, dtype='int')

p = pyaudio.PyAudio()

stream = p.open(
    format=FORMAT,
    channels=CHANNELS,
    rate=RATE,
    input=True,
    frames_per_buffer=CHUNK
)

SCR = curses.initscr()
curses.curs_set(0)
curses.start_color()
for i, color_pair in enumerate(COLOR_THEME):
    curses.init_pair(1 + i, *color_pair)
max_val = 0

def plot_soundcloud(values, height):
    """Plot the values to the screen as vertical bars."""
    global max_val
    max_val = max(max_val, values[-1])
    SCR.clear()
    for x, val in enumerate(values[:-1]):
        for y in range(height):
            c = min(len(COLOR_THEME), (y * len(COLOR_THEME)) // height) + 1
            if val <= y * (max_val // height):
                break
            try:
                SCR.addch(height - y - 1, x, ord('@'), curses.color_pair(c))
            except Exception:
                curses.nocbreak()
                SCR.keypad(0)
                curses.echo()
                curses.endwin()
                stream.close()
                p.terminate()
                print(
                    'x', x,
                    'y', y,
                    'c', c,
                    'h', height,
                    'l', values.shape[0],
                )
                print(values)
                exit()


    SCR.refresh()


try:
    while True:
        data = stream.read(CHUNK)
        frames = np.array(wave.struct.unpack("%dh"%(len(data)/2), data))
        m = int(np.sqrt(sum(np.square(frames, dtype='int32')) / len(frames)))
        scr_height, scr_width = SCR.getmaxyx()
        if RBUF.shape[0] < scr_width:
            RBUF = np.concatenate((np.zeros(scr_width - RBUF.shape[0] - 1, dtype='int'), RBUF, [0]))
        elif RBUF.shape[0] > scr_width:
            RBUF = RBUF[-scr_width:]
        RBUF[-1] = m
        plot_soundcloud(RBUF, height=scr_height)
        RBUF = np.roll(RBUF, -1)
except KeyboardInterrupt:
    curses.nocbreak()
    SCR.keypad(0)
    curses.echo()
    curses.endwin()
    stream.stop_stream()
    stream.close()
    p.terminate()
